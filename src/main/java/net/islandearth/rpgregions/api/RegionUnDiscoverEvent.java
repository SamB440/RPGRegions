package net.islandearth.rpgregions.api;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import lombok.Getter;
import net.islandearth.rpgregions.api.data.Discovery;

/** Called when a region is discovered (Right click removal in GUI)
 */
public class RegionUnDiscoverEvent extends Event {

	@Getter Discovery d;
	
    private static final HandlerList handlers = new HandlerList();
    
    public RegionUnDiscoverEvent(Discovery d)
    {
    	this.d = d;
    }
	
	public HandlerList getHandlers() 
	{
		return handlers;
	}
	
	public static HandlerList getHandlerList() 
	{
		return handlers;
	}
}
