package com.SamB440.RPG.Regions.commands;

import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.SamB440.RPG.Regions.Main;
import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import net.islandearth.rpgregions.utils.Utils;
import net.md_5.bungee.api.ChatColor;

public class Discovered implements CommandExecutor {
	
	Main plugin;
	
	public Discovered(Main plugin)
	{
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if(sender instanceof Player) 
		{
			Player p = (Player)sender;
			if(args.length == 0)
			{
				Utils utils = new Utils(plugin);
				ArrayList<String> discs = utils.getAllDiscoveries(p);
				ArrayList<String> undiscs = utils.getAllRegionsNotDiscovered(p);
				Inventory i = Bukkit.createInventory(null, 54, "Discoveries");
				ItemStack np = new ItemStack(Material.PURPLE_STAINED_GLASS_PANE);
				ItemMeta npmeta = np.getItemMeta();
				npmeta.setDisplayName(ChatColor.LIGHT_PURPLE + "Next page");
				np.setItemMeta(npmeta);
				i.setItem(53, np);
				ItemStack pp = new ItemStack(Material.ORANGE_STAINED_GLASS_PANE);
				ItemMeta ppmeta = pp.getItemMeta();
				ppmeta.setDisplayName(ChatColor.GOLD + "Previous page");
				pp.setItemMeta(ppmeta);
				i.setItem(45, pp);
				for(String s : discs)
				{
					ItemStack item = new ItemStack(Material.GREEN_STAINED_GLASS_PANE);
					ItemMeta imeta = item.getItemMeta();
					imeta.setDisplayName(ChatColor.GREEN + plugin.getConfig().getString("Messages." + s + ".Name"));
					ProtectedRegion r = WGBukkit.getRegionManager(p.getWorld()).getRegion(s);
					Location l = getCenterOfRegion(p.getWorld(), r);
					if(p.hasPermission("RPGRegions.removediscovery")) 
					{
						if(p.hasPermission("RPGRegions.showloc")) imeta.setLore(Arrays.asList(ChatColor.WHITE + "Discovered on: " + utils.getDateOfDiscovery(p, s), ChatColor.WHITE + "X: " + l.getX() + " Y: " + l.getY() + " Z: " + l.getZ(), ChatColor.GRAY + "Right click to remove discovery"));
						else imeta.setLore(Arrays.asList(ChatColor.WHITE + "Discovered on: " + utils.getDateOfDiscovery(p, s), ChatColor.GRAY + "Right click to remove discovery"));
					} else if(!p.hasPermission("RPGRegions.removediscovery")) {
						if(p.hasPermission("RPGRegions.showloc")) imeta.setLore(Arrays.asList(ChatColor.WHITE + "Discovered on: " + utils.getDateOfDiscovery(p, s), ChatColor.WHITE + "X: " + l.getX() + " Y: " + l.getY() + " Z: " + l.getZ()));
						else imeta.setLore(Arrays.asList(ChatColor.WHITE + "Discovered on: " + utils.getDateOfDiscovery(p, s)));
					}
					item.setItemMeta(imeta);
					i.addItem(item);
				}
				if(p.hasPermission("RPGRegions.seeundiscovered"))
				{
					for(String s : undiscs)
					{
						ItemStack item = new ItemStack(Material.RED_STAINED_GLASS_PANE);
						ItemMeta imeta = item.getItemMeta();
						imeta.setDisplayName(ChatColor.RED + plugin.getConfig().getString("Messages." + s + ".Name"));
						imeta.setLore(Arrays.asList(ChatColor.WHITE + "Not yet discovered!"));
						item.setItemMeta(imeta);
						i.addItem(item);
					}
				}
				p.openInventory(i);
			}
		}
		return true;
	}
	private Location getCenterOfRegion(World w, ProtectedRegion r)
	{
		//Get top location
        Location top = new Location(w, 0, 0, 0);
        top.setX(r.getMaximumPoint().getX());
        top.setY(r.getMaximumPoint().getY());
        top.setZ(r.getMaximumPoint().getZ());
       
        //Get bottom location
        Location bottom = new Location(w, 0, 0, 0);
        bottom.setX(r.getMinimumPoint().getX());
        bottom.setY(r.getMinimumPoint().getY());
        bottom.setZ(r.getMinimumPoint().getZ());
       
        //Split difference
        double X =  ((bottom.getX() - top.getX())/2) + bottom.getX();
        double Y =  ((bottom.getY() - top.getY())/2) + bottom.getY();
        double Z =  ((bottom.getZ() - top.getZ())/2) + bottom.getZ();
       
        //Setup new location
        return new Location(w, X, Y, Z);
	}
}
