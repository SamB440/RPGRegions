package com.SamB440.RPG.Regions.listeners;

import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.SamB440.RPG.Regions.RPGRegions;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import net.islandearth.rpgregions.api.RegionUnDiscoverEvent;
import net.islandearth.rpgregions.api.data.Discovery;
import net.islandearth.rpgregions.utils.Utils;

public class InventoryListener implements Listener {
	
	private ProtectedRegion region;
	private RPGRegions plugin;
	private Inventory editor = Bukkit.createInventory(null, 9, "Region Editor");
	
	public InventoryListener(RPGRegions plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onClick(InventoryClickEvent ice) {
		Inventory i = ice.getInventory();
		Player p = (Player) ice.getWhoClicked();
		if (i != null && ice.getCurrentItem() != null && ice.getCurrentItem().getType() != Material.AIR) {
			Utils utils = new Utils(plugin);
		    WorldGuard wgPlugin = WorldGuard.getInstance();
			RegionManager rm = wgPlugin.getPlatform().getRegionContainer().get(BukkitAdapter.adapt(p.getWorld()));
			if (ice.getView().getTitle().equals("Discoveries")) {
				ArrayList<String> undiscs = utils.getAllRegionsNotDiscovered(p);
				ArrayList<ItemStack> unitems = new ArrayList<>();
				for(String s : undiscs)
				{
					ItemStack item = new ItemStack(Material.RED_STAINED_GLASS_PANE);
					ItemMeta imeta = item.getItemMeta();
					imeta.setDisplayName(ChatColor.RED + plugin.getConfig().getString("Messages." + s + ".Name"));
					imeta.setLore(Arrays.asList(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("Messages." + s + ".Not_Discovered"))));
					item.setItemMeta(imeta);
					unitems.add(item);
				}
				if(ice.getClick().equals(ClickType.RIGHT) && !unitems.contains(ice.getCurrentItem()) && p.hasPermission("RPGRegions.removediscovery"))
				{
					String itemname = ChatColor.stripColor(ice.getCurrentItem().getItemMeta().getDisplayName());
					for(String s : plugin.getConfig().getStringList("Tasks.Region.Valid_Regions"))	
					{
						if(plugin.getConfig().getString("Messages." + s + ".Name").equals(itemname))
						{
							//String itemname = ChatColor.stripColor(ice.getCurrentItem().getItemMeta().getDisplayName());
							String date = utils.getDateOfDiscovery(p, s);
							
							Discovery d = new Discovery(rm.getRegion(s), p, date, plugin);
							d.delete();
							
							//Utils.removeDiscovery(itemname + ":" + p.getUniqueId() + "" + date);
							Bukkit.getPluginManager().callEvent(new RegionUnDiscoverEvent(d));
							p.closeInventory();
							p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("Messages.Region.Removed")));
						}
					}
				}
				ice.setCancelled(true);
			}
		}
	}
}
