package net.islandearth.rpgregions;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.SamB440.RPG.Regions.commands.Discovered;
import com.SamB440.RPG.Regions.commands.RPGRegionsCommand;
import com.SamB440.RPG.Regions.listeners.InventoryListener;
import com.SamB440.RPG.Regions.listeners.RegionListener;

import lombok.Getter;
import net.islandearth.rpgregions.metrics.Metrics;
import net.islandearth.rpgregions.utils.Utils;

public class RPGRegionsOld extends JavaPlugin {
	
    @Getter 
    private Connection SQL;
    
    private String host, database, username, password;
    private int port;
	
	Logger log = Bukkit.getLogger();
	String c = "[RPGRegions] ";
	
	@Override
	public void onEnable() {
		log.info(ChatColor.GREEN + "------ Loading RPG-Regions ------");
		if (Bukkit.getPluginManager().getPlugin("WorldGuard") != null) {
			File f = new File("plugins/RPGRegions/Storage/");
			if(!f.exists()) f.mkdir();
			log.info(c + "WorldGuard found!");
			getConfig().options().copyDefaults(true);
			createConfig();
			addFiles();
			registerCommands();
			registerListeners();
			startMetrics();
			startTasks();
		} else {
			log.severe(ChatColor.RED + "---------------------------------------------");
			log.severe(c + "WorldGuard has not been found! You must install it to use this plugin.");
			log.severe(c + "The plugin will now disable.");
			log.severe(ChatColor.RED + "---------------------------------------------");
			Bukkit.getPluginManager().disablePlugin(this);
		}
		
		if (getConfig().getBoolean("Server.MySQL.Enabled")) {
	        host = getConfig().getString("Server.MySQL.Host");
	        port = getConfig().getInt("Server.MySQL.Port");
	        database = getConfig().getString("Server.MySQL.Database");
	        username = getConfig().getString("Server.MySQL.Username");
	        password = getConfig().getString("Server.MySQL.Password");
	        try {    
	            openConnection();
				createDiscoveryTable();
				log.info(c + "Opened connection to MySQL server and created Discovery table!");
	        } catch (ClassNotFoundException e) {
	            e.printStackTrace();
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
		}
	}
	
	@Override
	public void onDisable() {
		if (getConfig().getBoolean("Server.MySQL.Enabled")) {
			try {
				SQL.close();
				log.info(c + "Closed MySQL connection!");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void openConnection() throws SQLException, ClassNotFoundException {
	    if (SQL != null && !SQL.isClosed()) 
	    {
	        return;
	    }
	    synchronized (this) 
	    {
	        if (SQL != null && !SQL.isClosed()) 
	        {
	            return;
	        }
	        Class.forName("com.mysql.jdbc.Driver");
	        SQL = DriverManager.getConnection("jdbc:mysql://" + this.host+ ":" + this.port + "/" + this.database + "?autoReconnect=true&useSSL=false", this.username, this.password);
	    }
	}
	
    public void createDiscoveryTable() {
        try {
 
            Statement statement = SQL.createStatement();
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS DiscoveryDatabase (REGION VARCHAR(36) NOT NULL, UUID VARCHAR(36) NOT NULL, DATE VARCHAR(16) NOT NULL)");
            //statement.executeUpdate("INSERT INTO DiscoveryDatabase(Region, UUID, String) VALUES('TestRegion','4b319cd4-e827-4dcf-a303-9a3fce310755', '13/01/2018 19:33')");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void dropDiscoveryTable() {
    	try {
    		Statement statement = SQL.createStatement();
    		statement.executeUpdate("DROP TABLE IF EXISTS DiscoveryDatabase");
    	} catch (SQLException e) {
    		e.printStackTrace();
    	}
    }
    
	private void createConfig()
	{
		log.info(ChatColor.GREEN + "------ Creating config ------");
		getConfig().addDefault("Tasks.Region.Interval", 200);
		getConfig().addDefault("Tasks.Region.Valid_Regions", Arrays.asList("Spawn", "test"));
		getConfig().addDefault("Messages.Region.Enabled", true);
		getConfig().addDefault("Messages.Region.Removed", "&cDiscovery removed.");
		getConfig().addDefault("Messages.Region.Format", "dd/MM/yyyy HH:mm");
		getConfig().addDefault("Server.MySQL.Enabled", false);
		getConfig().addDefault("Server.MySQL.Host", "localhost");
		getConfig().addDefault("Server.MySQL.Port", 3306);
		getConfig().addDefault("Server.MySQL.Database", "TestDatabase");
		getConfig().addDefault("Server.MySQL.Username", "User");
		getConfig().addDefault("Server.MySQL.Password", "Pass");
		for(String s : getConfig().getStringList("Tasks.Region.Valid_Regions"))
		{
			getConfig().addDefault("Messages." + s + ".Discovered", Arrays.asList("&m---------------------------------------------------", "&aCongratulations, &9{player}!", "&7You have discovered &c{region}.", "&7Continue exploring to discover more!", "&m---------------------------------------------------"));
			getConfig().addDefault("Messages." + s + ".Discovered_Title", "&a{region} discovered.");
			getConfig().addDefault("Messages." + s + ".Discovered_Subtitle", "&fContinue exploring to discover more!");
			getConfig().addDefault("Messages." + s + ".Not_Discovered", Arrays.asList("Not yet discovered!"));
			getConfig().addDefault("Rewards." + s + ".Experience", 10);
			getConfig().addDefault("Rewards." + s + ".Item.Type", "DIAMOND");
			getConfig().addDefault("Rewards." + s + ".Item.Amount", 2);
			getConfig().addDefault("Rewards." + s + ".Item.Name", "Diamond");
			getConfig().addDefault("Rewards." + s + ".Item.Lore", Arrays.asList("A special kind of diamond.", "Might have special powers."));
			getConfig().addDefault("Sounds." + s + ".Discover", "ENTITY_PLAYER_LEVELUP");
			getConfig().addDefault("Commands." + s + ".Enabled", true);
			getConfig().addDefault("Commands." + s + ".Execute", "me just discovered a region!");
			getConfig().addDefault("Commands." + s + ".Type", "PLAYER");
			getConfig().addDefault("Messages." + s + ".Name", s);
		}
		saveConfig();
	}
	
    private void registerCommands() 
    {
        this.getCommand("RPGRegions").setExecutor(new RPGRegionsCommand(this));
        this.getCommand("Discoveries").setExecutor(new Discovered(this));
    }
    
    private void registerListeners()
    {
    	PluginManager pm = Bukkit.getPluginManager();
    	pm.registerEvents(new InventoryListener(this), this);
    }
    
	private void addFiles()
	{
		Utils utils = new Utils(this);
		File file = new File(this.getDataFolder() + "/storage/");
		File file2 = new File(this.getDataFolder() + "/storage/discoveries.yml");
		if (!file.exists()) {
			file.mkdir();
		}
		
		if (!file2.exists()) {
			try {
				file2.createNewFile();
				utils.addDiscovery("* WARNING * Do not edit this file unless you know what you are doing! Editing this file can result in corruption of discovery data.");
				utils.addDiscovery("* WARNING * You should make regular backups of this file to prevent loss of discoveries.");
				log.info(c + "Done!");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void startMetrics() {
		Metrics metrics = new Metrics(this);
		metrics.addCustomChart(new Metrics.SingleLineChart("regions_discovered", () -> new Utils(this).getTotalDiscoveries()));
		log.info(c + "Started metrics. Opt out using global bStats config.");
	}
	
	private void startTasks() {
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new RegionListener(this), 0, getConfig().getInt("Tasks.Region.Interval"));
	}
	
	public boolean isLatest() {
		if (Bukkit.getServer().getVersion().contains("1.14")) return true;
		else return false;
	}
}
