package com.SamB440.RPG.Regions.listeners;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.SamB440.RPG.Regions.Main;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.protection.regions.RegionQuery;

import net.islandearth.rpgregions.api.RegionDiscoverEvent;
import net.islandearth.rpgregions.api.data.CommandType;
import net.islandearth.rpgregions.api.data.Discovery;
import net.islandearth.rpgregions.utils.Utils;

public class RegionListener implements Runnable {
	
	private ArrayList<String> regions = new ArrayList<>();
	private Main plugin;
	
	public RegionListener(Main plugin) {
		this.plugin = plugin;
	}
	
	public void updateRegions() {
		regions.clear();
		for (String s : plugin.getConfig().getStringList("Tasks.Region.Valid_Regions")) {
			regions.add(s);
		}
	}

	@Override
	public void run() {
		if (Bukkit.getOnlinePlayers().size() > 0) {
			updateRegions();
    		for (Player p : Bukkit.getOnlinePlayers()) {
    			RegionQuery q = WorldGuard.getInstance().getPlatform().getRegionContainer().createQuery();
    	        ApplicableRegionSet ars = q.getApplicableRegions(BukkitAdapter.adapt(p.getLocation()));
    			for (ProtectedRegion r : ars.getRegions()) {
    				if (regions.contains(r.getId()) && !new Utils(plugin).readDiscoveryFile(r.getId() + ":" + p.getUniqueId(), plugin.getConfig().getBoolean("Server.MySQL.Enabled"), p, r.getId())) {
    					ItemStack item = new ItemStack(Material.getMaterial(plugin.getConfig().getString("Rewards." + r.getId() + ".Item.Type")), plugin.getConfig().getInt("Rewards." + r.getId() + ".Item.Amount"));
    					ItemMeta imeta = item.getItemMeta();
    					imeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("Rewards." + r.getId() + ".Item.Name")));
    					imeta.setLore(plugin.getConfig().getStringList("Rewards." + r.getId() + ".Item.Lore"));
    					item.setItemMeta(imeta);
    					p.getInventory().addItem(item);
    					p.playSound(p.getLocation(), Sound.valueOf(plugin.getConfig().getString("Sounds." + r.getId() + ".Discover")), 1, 1);
    					p.giveExp(plugin.getConfig().getInt("Rewards." + r.getId() + ".Experience"));
    					if(plugin.getConfig().getBoolean("Messages.Region.Enabled"))
    					{
    						//p.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("Messages." + r.getId() + ".Discovered").replace("{region}", r.getId())));
    						String title = ChatColor.translateAlternateColorCodes('&',plugin.getConfig().getString("Messages." + r.getId() + ".Discovered_Title"));
    						String subtitle = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("Messages." + r.getId() + ".Discovered_Subtitle"));
       						String[] search = {"{region}", "{player}"};
    						String[] replace = {r.getId(), p.getName()};
    						title = StringUtils.replaceEach(title, search, replace);
    						subtitle = StringUtils.replaceEach(subtitle, search, replace);
    						if(plugin.isLatest()) p.sendTitle(title, subtitle, 40, 175, 40);
    						for(String s : plugin.getConfig().getStringList("Messages." + r.getId() + ".Discovered"))
    						{
    							p.sendMessage(ChatColor.translateAlternateColorCodes('&', StringUtils.replaceEach(s, search, replace)));
    						}
    					}
    					
    					if(plugin.getConfig().getBoolean("Commands." + r.getId() + ".Enabled"))
    					{
    						CommandType ct = CommandType.valueOf(plugin.getConfig().getString("Commands." + r.getId() + ".Type"));
    						if(ct.equals(CommandType.PLAYER)) p.performCommand(plugin.getConfig().getString("Commands." + r.getId() + ".Execute"));
    						else if(ct.equals(CommandType.SERVER)) Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), plugin.getConfig().getString("Commands." + r.getId() + ".Execute"));
    					}
    					DateFormat dateFormat = new SimpleDateFormat(plugin.getConfig().getString("Messages.Region.Format"));
    					Date date = new Date();
    					Discovery d = new Discovery(r, p, item, dateFormat.format(date), plugin);
    					d.create();
    					//Utils.addDiscovery(r.getId() + ":" + p.getUniqueId() + " " + dateFormat.format(date));
        				Bukkit.getPluginManager().callEvent(new RegionDiscoverEvent(d));
    				}
    			}
    		}
        }
	}
}
