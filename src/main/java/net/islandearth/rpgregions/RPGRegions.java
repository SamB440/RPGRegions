package net.islandearth.rpgregions;

import java.io.File;
import java.io.IOException;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import lombok.Getter;
import net.islandearth.languagy.language.Language;
import net.islandearth.languagy.language.LanguagyImplementation;
import net.islandearth.languagy.language.LanguagyPluginHook;
import net.islandearth.languagy.language.Translator;

public class RPGRegions extends JavaPlugin implements LanguagyPluginHook {
	
	@LanguagyImplementation(fallbackFile = "plugins/RPGRegions/lang")
	@Getter
	private Translator translator;
	
	@Override
	public void onEnable() {
		for (Language language : Language.values()) {
			File file = new File(this.getDataFolder() + "/lang/" + language.getCode() + ".yml");
			if (!file.exists()) {
				try {
					file.createNewFile();
					FileConfiguration config = YamlConfiguration.loadConfiguration(file);
					config.options().copyDefaults(true);
					config.addDefault("lang", "whatever");
					config.save(file);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	@Override
	public void onDisable() {
		
	}

	@Override
	public void onLanguagyHook() {
		translator.setDebug(true);
		translator.setDisplay(Material.MAP);
		translator.getOptions().externalDirectory("https://www.islandearth.net/plugins/RPGRegions/lang");
	}

}
