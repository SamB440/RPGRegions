package net.islandearth.rpgregions.api.data;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.utils.Utils;

public class Discovery {
	
	private ProtectedRegion r;
	private Player p;
	private ItemStack i;
	private String d;
	private DateFormat dateFormat;
	private RPGRegions plugin;

	public Discovery(ProtectedRegion r, Player p, ItemStack i, String d, RPGRegions plugin) {
    	this.r = r;
    	this.p = p;
    	this.i = i;
    	this.d = d;
    	this.dateFormat = new SimpleDateFormat(plugin.getConfig().getString("Messages.Region.Format"));
    	this.plugin = plugin;
	}
	
	public Discovery(ProtectedRegion r, Player p, String d, RPGRegions plugin) {
    	this.r = r;
    	this.p = p;
    	this.i = null;
    	this.d = d;
    	this.dateFormat = new SimpleDateFormat(plugin.getConfig().getString("Messages.Region.Format"));
    	this.plugin = plugin;
	}
	
	public ProtectedRegion getRegion() {
		return r;
	}
	
	public Player getPlayer() {
		return p;
	}
	
	public ItemStack getGivenItem() {
		return i;
	}
	
	public String getTimeOfDiscovery() {
		return d;
	}
	
	public void create() {
		if(plugin.getConfig().getBoolean("Server.MySQL.Enabled")) 
		{
			try {
				PreparedStatement statement = plugin.getSQL().prepareStatement("INSERT INTO DiscoveryDatabase(Region, UUID, Date) VALUES(?, ?, ?)");
				statement.setString(1, r.getId());
				statement.setString(2, p.getUniqueId().toString().replace("-", ""));
				statement.setString(3, d);
				statement.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else new Utils(plugin).addDiscovery(r.getId() + ":" + p.getUniqueId() + "" + d);
	}
	
	public void delete() {
		if(plugin.getConfig().getBoolean("Server.MySQL.Enabled")) 
		{
			try {
				PreparedStatement statement = plugin.getSQL().prepareStatement("DELETE FROM DiscoveryDatabase WHERE REGION = ? AND UUID = ?");
				statement.setString(1, r.getId());
				statement.setString(2, p.getUniqueId().toString().replace("-", ""));
				statement.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else new Utils(plugin).removeDiscovery(r.getId() + ":" + p.getUniqueId() + "" + d);
	}
}
