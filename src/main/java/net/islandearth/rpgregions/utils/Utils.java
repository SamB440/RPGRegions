package net.islandearth.rpgregions.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import lombok.AllArgsConstructor;
import net.islandearth.rpgregions.RPGRegions;

@AllArgsConstructor
public class Utils {

	private RPGRegions plugin;
	
	public void addDiscovery(Object obj) {
		File file = new File(plugin.getDataFolder() + "/storage/discoveries.yml");
		FileConfiguration config = YamlConfiguration.loadConfiguration(file);
		
		try {
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("plugins/RPGRegions/Storage/Discoveries.txt", true)));
			out.println(obj.toString());
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	    
	public void removeDiscovery(Object obj) {
		try  {
			File inputFile = new File("plugins/RPGRegions/Storage/Discoveries.txt");
			if (!inputFile.isFile()) {
				System.out.println("File does not exist.");
				return;
			}
            File tempFile = new File(inputFile.getAbsolutePath() + ".tmp");
            BufferedReader br = new BufferedReader(new FileReader("plugins/RPGRegions/Storage/Discoveries.txt"));
            PrintWriter pw = new PrintWriter(new FileWriter(tempFile));
            String line = null;
            while ((line = br.readLine()) != null) {
                if (!line.trim().equals(obj.toString())) {
                    pw.println(line);
                    pw.flush();
                }
            }
            pw.flush();
            pw.close();
            br.close();
            if (!inputFile.delete()) {
                System.out.println("Could not delete file.");
                return;
            }
            if (!tempFile.renameTo(inputFile))
            {
                System.out.println("Could not rename file.");
            }
		}
        catch (FileNotFoundException ex) 
		{
            ex.printStackTrace();
        } catch (IOException ex) 
		{
            ex.printStackTrace();
        }
    }
	
	public boolean readDiscoveryFile(Object obj, boolean mysql, Player p, String region) {
		Boolean bool_discovery_present = false;
		if(mysql)
		{
			/*try {
				Statement statement = plugin.getSQL().createStatement();
				if(statement.execute("IF EXISTS (SELECT * FROM DiscoveryDatabase WHERE REGION = " + region + " AND UUID = " + p.getUniqueId() + ")"))
				{
					bool_discovery_present = true;
				} else bool_discovery_present = false;
				//ResultSet rs = statement.getResultSet();
				//bool_discovery_present = rs.first();
			} catch (SQLException e) {
				e.printStackTrace();
			}*/
			try {
				PreparedStatement statement = plugin.getSQL().prepareStatement("SELECT REGION FROM DiscoveryDatabase WHERE REGION = ? AND UUID = ?");
				statement.setString(1, region);
				statement.setString(2, p.getUniqueId().toString().replace("-", ""));
				ResultSet rs = statement.executeQuery();
				if(rs.next()) return rs.getString("REGION").equals(region);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			try 
			{
				File fileToRead = new File("plugins/RPGRegions/Storage/Discoveries.txt");
				BufferedReader br = new BufferedReader(new FileReader(fileToRead));
				String line = null;
				line = br.readLine();
				while ((line = br.readLine()) != null && !bool_discovery_present) 
				{
					bool_discovery_present=line.contains(obj.toString());
				}
				br.close();
			} catch (FileNotFoundException e1) {
				System.out.print("[RPGRegions] No file is available!");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		return bool_discovery_present;
	}
	
	public ArrayList<String> getAllDiscoveries(Player p) {
		ArrayList<String> l = new ArrayList<>();
		for(String s : plugin.getConfig().getStringList("Tasks.Region.Valid_Regions"))
		{
			if(readDiscoveryFile(s + ":" + p.getUniqueId(), plugin.getConfig().getBoolean("Server.MySQL.Enabled"), p, s))
			{
				l.add(s);
			}
		}
		return l;
	}
	
	public ArrayList<String> getAllRegionsNotDiscovered(Player p) {
		ArrayList<String> l = new ArrayList<>();
		for(String s : plugin.getConfig().getStringList("Tasks.Region.Valid_Regions"))
		{
			if(!readDiscoveryFile(s + ":" + p.getUniqueId(), plugin.getConfig().getBoolean("Server.MySQL.Enabled"), p, s))
			{
				l.add(s);
			}
		}
		return l;
	}
	
	public String getDateOfDiscovery(Player p, String region) {
		for(String s : plugin.getConfig().getStringList("Tasks.Region.Valid_Regions"))
		{
			if(plugin.getConfig().getBoolean("Server.MySQL.Enabled"))
			{
				try {
					PreparedStatement statement = plugin.getSQL().prepareStatement("SELECT DATE FROM DiscoveryDatabase WHERE REGION = ? AND UUID = ?");
					statement.setString(1, region);
					statement.setString(2, p.getUniqueId().toString().replace("-", ""));
					ResultSet rs = statement.executeQuery();
					if(rs.next()) return rs.getString("date");
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			else
			{
				try 
				{
					File fileToRead = new File("plugins/RPGRegions/Storage/Discoveries.txt");
					BufferedReader br = new BufferedReader(new FileReader(fileToRead));
					String line = null;
					line = br.readLine();
					while ((line = br.readLine()) != null) 
					{
						if(line.contains(s + ":" + p.getUniqueId()))
						{
							br.close();
							return line.replace(s + ":" + p.getUniqueId(), "");
						}
					}
					br.close();
					} catch (FileNotFoundException e1) {
						System.out.print("[RPGRegions] No file is available!");
					} catch (IOException e1) {
							e1.printStackTrace();
				}
			}
		}
		return null;
	}
	
	public int getTotalDiscoveries() throws IOException {
	    LineNumberReader reader = null;
	    try {
	        reader = new LineNumberReader(new FileReader("plugins/RPGRegions/Storage/Discoveries.txt"));
	        while ((reader.readLine()) != null);
	        return reader.getLineNumber() - 2;
	    } catch (Exception ex) {
	        return -1;
	    } finally { 
	        if(reader != null)
	            reader.close();
	    }
	}
}
