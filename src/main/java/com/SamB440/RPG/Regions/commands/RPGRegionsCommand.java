package com.SamB440.RPG.Regions.commands;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.SamB440.RPG.Regions.Main;
import com.SamB440.RPG.Regions.RPGRegions;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import lombok.AllArgsConstructor;
import net.islandearth.rpgregions.api.data.Discovery;
import net.islandearth.rpgregions.utils.Utils;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

@AllArgsConstructor
public class RPGRegionsCommand implements CommandExecutor {

	private RPGRegions plugin;
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if(sender instanceof Player) 
		{
			Player p = (Player)sender;
			if(args.length == 0)
			{
				sendPageOne(p);
			}
			else if(args.length == 1)
			{
				if(args[0].equalsIgnoreCase("2"))
				{
					sendPageTwo(p);
				} else if(args[0].equalsIgnoreCase("1")) {
					sendPageOne(p);
				} else if(args[0].equalsIgnoreCase("reload") && p.isOp()) {
					p.sendMessage(ChatColor.GREEN + "Reloading...");
					plugin.reloadConfig();
					plugin.saveConfig();
					p.sendMessage(ChatColor.GREEN + "Done!");
				} else p.sendMessage(ChatColor.RED + "Unknown sub-command.");
			} else if(args.length == 2) {
				switch(args[0].toLowerCase())
				{
					case "remove":
						if(p.hasPermission("RPGRegions.removediscovery"))
						{
							String date = new Utils(plugin).getDateOfDiscovery(p, args[1]);
						    WorldGuard wgPlugin = WorldGuard.getInstance();
							RegionManager rm = wgPlugin.getPlatform().getRegionContainer().get(BukkitAdapter.adapt(p.getWorld()));
							Discovery d = new Discovery(rm.getRegion(args[1]), p, date, plugin);
							d.delete();
							//Utils.removeDiscovery(args[2] + ":" + p.getUniqueId() + "" + date);
						}
						
						break;
						
					case "addregion":
						if(p.hasPermission("RPGRegions.addregion"))
						{
						    WorldGuard wgPlugin = WorldGuard.getInstance();
							RegionManager rm = wgPlugin.getPlatform().getRegionContainer().get(BukkitAdapter.adapt(p.getWorld()));
							ProtectedRegion r = rm.getRegion(args[1]);
							String s = r.getId();
							List<String> regions = plugin.getConfig().getStringList("Tasks.Region.Valid_Regions");
							regions.add(s);
							plugin.getConfig().set("Tasks.Region.Valid_Regions", regions);
							plugin.getConfig().set("Messages." + s + ".Discovered", Arrays.asList("&m---------------------------------------------------", "&aCongratulations, &9{player}!", "&7You have discovered &c{region}.", "&7Continue exploring to discover more!", "&m---------------------------------------------------"));
							plugin.getConfig().set("Messages." + s + ".Discovered_Title", "&a{region} discovered.");
							plugin.getConfig().set("Messages." + s + ".Discovered_Subtitle", "&fContinue exploring to discover more!");
							plugin.getConfig().set("Messages." + s + ".Not_Discovered", Arrays.asList("Not yet discovered!"));
							plugin.getConfig().set("Rewards." + s + ".Experience", 10);
							plugin.getConfig().set("Rewards." + s + ".Item.Type", "DIAMOND");
							plugin.getConfig().set("Rewards." + s + ".Item.Amount", 2);
							plugin.getConfig().set("Rewards." + s + ".Item.Name", "Diamond");
							plugin.getConfig().set("Rewards." + s + ".Item.Lore", Arrays.asList("A special kind of diamond.", "Might have special powers."));
							plugin.getConfig().set("Sounds." + s + ".Discover", "ENTITY_PLAYER_LEVELUP");
							plugin.getConfig().set("Commands." + s + ".Enabled", true);
							plugin.getConfig().set("Commands." + s + ".Execute", "me just discovered a region!");
							plugin.getConfig().set("Commands." + s + ".Type", "PLAYER");
							plugin.getConfig().set("Messages." + s + ".Name", s);
							plugin.saveConfig();
							plugin.reloadConfig();
							p.sendMessage(ChatColor.GREEN + "Added region!");
						}
						
						break;
						
					case "removeregion":
						
						if(sender.hasPermission("rpgregions.removeregion"))
						{
						    WorldGuard wgPlugin = WorldGuard.getInstance();
							RegionManager rm = wgPlugin.getPlatform().getRegionContainer().get(BukkitAdapter.adapt(p.getWorld()));
							ProtectedRegion r = rm.getRegion(args[1]);
							String s = r.getId();
							List<String> regions = plugin.getConfig().getStringList("Tasks.Region.Valid_Regions");
							regions.remove(s);
							plugin.getConfig().set("Tasks.Region.Valid_Regions", regions);
							plugin.getConfig().set("Messages." + s + ".Discovered", null);
							plugin.getConfig().set("Messages." + s + ".Discovered_Title", null);
							plugin.getConfig().set("Messages." + s + ".Discovered_Subtitle", null);
							plugin.getConfig().set("Messages." + s + ".Not_Discovered", null);
							plugin.getConfig().set("Rewards." + s + ".Experience", null);
							plugin.getConfig().set("Rewards." + s + ".Item.Type", null);
							plugin.getConfig().set("Rewards." + s + ".Item.Amount", null);
							plugin.getConfig().set("Rewards." + s + ".Item.Name", null);
							plugin.getConfig().set("Rewards." + s + ".Item.Lore", null);
							plugin.getConfig().set("Sounds." + s + ".Discover", null);
							plugin.getConfig().set("Commands." + s + ".Enabled", null);
							plugin.getConfig().set("Commands." + s + ".Execute", null);
							plugin.getConfig().set("Commands." + s + ".Type", null);
							plugin.getConfig().set("Messages." + s + ".Name", null);
							plugin.saveConfig();
							plugin.reloadConfig();
							p.sendMessage(ChatColor.GREEN + "Removed region!");
						}
						
						break;
						
					default:
						sender.sendMessage(ChatColor.RED + "Unknown sub-command.");
						break;
				}
			} else if(args.length == 3) {
				switch(args[0].toLowerCase())
				{
					case "description":
						
						if(sender.hasPermission("rpgregions.description"))
						{
						    WorldGuard wgPlugin = WorldGuard.getInstance();
							RegionManager rm = wgPlugin.getPlatform().getRegionContainer().get(BukkitAdapter.adapt(p.getWorld()));
							ProtectedRegion r = rm.getRegion(args[1]);
							String s = r.getId();
							
							String[] lines = args[2].split(",");
							plugin.getConfig().set("Messages." + s + ".Discovered", Arrays.asList(lines));
							plugin.saveConfig();
							plugin.reloadConfig();
						}
						
						break;
						
					default:
						sender.sendMessage(ChatColor.RED + "Unknown sub-command.");
						break;
				}
			}
		}
		return true;
	}
	
	private void sendPageOne(Player p) {
		TextComponent help = new TextComponent(ChatColor.YELLOW + "Showing help for RPG-Regions " + ChatColor.WHITE + "1/2");
		help.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.WHITE + "Showing page 1/2 click to go to the next page or use /rr 2.").create()));
		help.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/rr 2"));
		p.spigot().sendMessage(help);
		TextComponent c1 = new TextComponent(ChatColor.GREEN + "/RPGRegions");
		c1.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.WHITE + "Click to paste command.").create()));
		c1.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/RPGRegions"));
		p.spigot().sendMessage(c1);
		p.sendMessage(ChatColor.WHITE + "   Aliases: /rr, /rpgregion, /rpregions, rpgr, rpgreg, rreg.");
		p.sendMessage(ChatColor.WHITE + "   Description: Displays help page.");
		TextComponent c2 = new TextComponent(ChatColor.GREEN + "/RPGRegions reload");
		c2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.WHITE + "Click to paste command.").create()));
		c2.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/RPGRegions reload"));
		p.spigot().sendMessage(c2);
		p.sendMessage(ChatColor.WHITE + "   Aliases: None.");
		p.sendMessage(ChatColor.WHITE + "   Description: Reload the plugin.");
		p.sendMessage(ChatColor.WHITE + "   Permission(s): OP");
		TextComponent c3 = new TextComponent(ChatColor.GREEN + "/Discoveries");
		c3.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.WHITE + "Click to paste command.").create()));
		c3.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/Discoveries"));
		p.spigot().sendMessage(c3);
		p.sendMessage(ChatColor.WHITE + "   Aliases: /discovered, /discs, /ds.");
		p.sendMessage(ChatColor.WHITE + "   Description: View your discoveries.");
		p.sendMessage(ChatColor.WHITE + "   Permission(s): RPGRegions.Discoveries");
		TextComponent c4 = new TextComponent(ChatColor.GREEN + "/RPGRegions remove <discovery>");
		c4.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.WHITE + "Click to paste command.").create()));
		c4.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/RPGRegions remove "));
		p.spigot().sendMessage(c4);
		p.sendMessage(ChatColor.WHITE + "   Aliases: None.");
		p.sendMessage(ChatColor.WHITE + "   Description: Remove a discovery.");
		p.sendMessage(ChatColor.WHITE + "   Permission(s): RPGRegions.removediscovery");
		p.sendMessage(ChatColor.YELLOW + "© 2018 IslandEarth. Made with" + " ❤ " + "by SamB440.");
	}
	
	private void sendPageTwo(Player p) {
		for (int i = 0; i < 10; i++) {
			p.sendMessage(" ");
		}
		
		TextComponent help = new TextComponent(ChatColor.YELLOW + "Showing help for RPG-Regions " + ChatColor.WHITE + "2/2");
		help.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.WHITE + "Showing page 2/2 click to go to the previous page or use /rr 1.").create()));
		help.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/rr 1"));
		p.spigot().sendMessage(help);
		TextComponent c1 = new TextComponent(ChatColor.GREEN + "/RPGRegions addregion <region>");
		c1.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.WHITE + "Click to paste command.").create()));
		c1.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/RPGRegions addregion "));
		p.spigot().sendMessage(c1);
		p.sendMessage(ChatColor.WHITE + "   Aliases: None.");
		p.sendMessage(ChatColor.WHITE + "   Description: Adds the region specified in the third arg to the config.");
		p.sendMessage(ChatColor.WHITE + "   Permission(s): RPGRegions.addregion");
		TextComponent c2 = new TextComponent(ChatColor.GREEN + "/RPGRegions removeregion <region>");
		c2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.WHITE + "Click to paste command.").create()));
		c2.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/RPGRegions removeregion "));
		p.spigot().sendMessage(c2);
		p.sendMessage(ChatColor.WHITE + "   Aliases: None.");
		p.sendMessage(ChatColor.WHITE + "   Description: Removes the region specified in the third arg to the config.");
		p.sendMessage(ChatColor.WHITE + "   Permission(s): RPGRegions.removeregion");
		TextComponent c3 = new TextComponent(ChatColor.GREEN + "/RPGRegions description <region> <description>");
		c3.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.WHITE + "Click to paste command.").create()));
		c3.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/rpgregions description "));
		p.spigot().sendMessage(c3);
		p.sendMessage(ChatColor.WHITE + "   Aliases: None.");
		p.sendMessage(ChatColor.WHITE + "   Description: Edit a regions description.");
		p.sendMessage(ChatColor.WHITE + "   Permission(s): RPGRegions.description");
		p.sendMessage(ChatColor.YELLOW + "© 2018 IslandEarth. Made with" + " ❤ " + "by SamB440.");
	}
}
